package cz.vse.cafourek.logika;

/**
 *  Třída Avatar - třída představující postavu hráče.
 *
 *  Třída Avatar představuje životy a žízeň hráče.
 *  Třída obsahuje metody na snižování/zvyšování životů/žízně
 *
 *@author     Petr Cafourek
 *@version    pro školní rok 2019/2020
 */

public class Avatar {

    private int zivoty;
    private int zizen;
    private Hra hra;

    public Avatar(Hra hra)
    {
        this.zivoty = 100;
        this.zizen = 0;
        this.hra = hra;
    }

    public void snizeniZivotu(int pocet)
    {
        if(this.zivoty - pocet <= 0)
        {
            System.out.printf("Životy sníženy z %d/100 na %d/100\n\n", this.zivoty, 0);
            System.out.print("Zemřel jsi\n\n");
            this.zivoty = 0;
            this.hra.setKonecHry(true);
        }
        else
        {
            System.out.printf("Životy sníženy z %d/100 na %d/100\n\n", this.zivoty, this.zivoty - pocet);
            this.zivoty -= pocet;
        }
    }

    public void zvyseniZivotu(int pocet)
    {
        if(this.zivoty + pocet > 100)
        {
            System.out.printf("Životy zvýšeny z %d/100 na %d/100\n", this.zivoty, 100);
            this.zivoty = 100;
        }
        else
        {
            System.out.printf("Životy zvýšeny z %d/100 na %d/100\n", this.zivoty, this.zivoty + pocet);
            this.zivoty += pocet;
        }

    }

    public void zvyseniZizne(int pocet)
    {
        if(this.zizen == 100)
        {
            System.out.printf("Žízeň 100/100. Dehydratace tě pomalu zabíjí!\n\n");
            snizeniZivotu(20);
        }

        else if(this.zizen + pocet > 100)
        {
            System.out.printf("Žízeň zvýšena z %d/100 na %d/100\n\n", this.zizen, 100);
            this.zizen = 100;
        }
        else
        {
            System.out.printf("Žízeň zvýšena z %d/100 na %d/100\n\n", this.zizen, this.zizen + pocet);
            this.zizen += pocet;
        }

    }

    public void snizeniZizne(int pocet)
    {
        if(this.zizen - pocet <= 0)
        {
            System.out.printf("Žízeň snížena z %d/100 na %d/100\n", this.zizen, 0);
            this.zizen = 0;
        }
        else
        {
            System.out.printf("Žízeň snížena z %d/100 na %d/100\n", this.zizen, this.zizen - pocet);
            this.zizen -= pocet;
        }
    }

    public int getZizen()
    { return this.zizen; }
}
