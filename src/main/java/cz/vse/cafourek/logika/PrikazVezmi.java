package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazVezmi implementuje pro hru příkaz vezmi.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */
public class PrikazVezmi implements IPrikaz{

    private static final String NAZEV = "vezmi";
    HerniPlan plan;
    Avatar avatar;
    Batoh batoh;

    /**
     *  Konstruktor třídy
     *
     *  @param plan, ze kterého se bere aktuální místnost.
     *               avatar, kterému se bude snižovat žízeň.
     *                 batoh, do kterého se přemisťuje vec z prostoru.
     */
    public PrikazVezmi (HerniPlan plan, Avatar avatar, Batoh batoh)
    {
        this.plan = plan;
        this.avatar = avatar;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "Vezmi". Přesune věc z prostoru do batohu.
     *  Pokud již v batohu není místo, nebo jméno věci je špatně,
     *  vypíše se chybové hlášení.
     *
     *@param parametry - String jméno věci, která se má přesunout z místnosti do batohu
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry)
    {
        if (parametry.length > 0) {

            if(this.batoh.volneMistoBatoh() == false ) {
                return "Nemáš volné místo v batohu";
            }
            Prostor aktualniProstor = this.plan.getAktualniProstor();
            if(aktualniProstor.obsahujeProstorVec(parametry[0]) == false)
            {
                return parametry[0] + " tu nikde nevidím";
            }

            if(aktualniProstor.getObsahProstoru().get(parametry[0]) instanceof Truhla)
            {
                return "Truhlu nemůžeš vzít, protože je příliš těžká";
            }

            Vec vzataVec = aktualniProstor.vezmiVec(parametry[0]);
            this.batoh.vlozVec(vzataVec);

            aktualniProstor.getObsahProstoru().remove(parametry[0]);

            nehodaAvatara(this.avatar);
            vysileniAvatara(this.avatar);

            return vzataVec.getJmeno() + " vložen do batohu";
        }
        else {
            return "Musíš napsat co chceš vzít";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
