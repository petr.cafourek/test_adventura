package cz.vse.cafourek.logika;

import java.util.*;

/**
 *  Třída PrikazProzkoumej implementuje pro hru příkaz prozkoumej.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */
public class PrikazProzkoumej implements IPrikaz{

    private static final String NAZEV = "prozkoumej";
    private HerniPlan plan;
    private Avatar avatar;

    /**
     *  Konstruktor třídy
     *
     *  @param plan, ze kterého, získáváte aktualni mistnost.
     *                 avatar, kterému snižujeme životy nebo zvyšujete žízeň
     */
    public PrikazProzkoumej(HerniPlan plan, Avatar avatar) {
        this.plan = plan;
        this.avatar = avatar;
    }


    /**
     *  Provádí příkaz "prozkoumej". Nastavení viditelnosti všech východů místnosti a všech věcí a truhel v místnosti
     *  na true. Prostor "poklad" nelze tímto příkazem objevit.
     *  Pokud uživatel zadá parametr k příkazu, vypíše se chybové hlášení.
     *
     *@param parametry - bez parametrů
     *@return zpráva, kterou vypíše hra hráči se všemi nově objevenými věcmi/prostory
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Nechápu, proč jste zadal druhé slovo.";
        }
        else
        {
            Prostor aktualniProstor = this.plan.getAktualniProstor();
            vysileniAvatara(this.avatar);
            return aktualniProstor.setVychodyViditelne() + "\n" + aktualniProstor.setVeciVProstoruViditelne();
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
