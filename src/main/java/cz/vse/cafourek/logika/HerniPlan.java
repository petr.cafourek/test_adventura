package cz.vse.cafourek.logika;


import java.util.Random;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *  Vytváří věci v adventuře, náhodně je nastavuje viditelné/neciditelné a
 *  náhodně umisťuje věci do truhlel a prostorů
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    pro školní rok 2019/2020
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;

    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();


    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor vchod = new Prostor("vchod","vchod do chrámu, se třemi cestami", true);
        Prostor levaMistnost = new Prostor("levá_místnost", " se zbytkem sochy hadího boha", true);
        Prostor prostredniMistnost = new Prostor("prostřední_místnost", "tajuplná místnost s napůl zříceným stropem", true);
        Prostor pravaMistnost = new Prostor("pravá_místnost", "malá místnost na konci úzké klikaté chodby", true);
        Prostor chodba = new Prostor("chodba", "skrytá chodba, která vypadá jako zadní vchod", false);
        Prostor skladiste = new Prostor("skladiště", "rujny místnosti co bývala skladiště",false);
        Prostor dvere = new Prostor("dveře", "Našel jsi zamčené dveře. Po bližším prozkoumání rytin na " +
                "dveřích se ukázalo, že je potřeba najít tři klíče.\n" +
                "Jenom pokud člověk prokáže sílu svého ducha a získá " +
                "tři klíče, tak bude moci otevřít dveře a získat poklad", true);
        Prostor poklad = new Prostor("poklad", "bájná pokladnice hadího boha", false);

        // přiřazují se průchody mezi prostory (sousedící prostory)
        vchod.setVychod(levaMistnost);
        vchod.setVychod(prostredniMistnost);
        vchod.setVychod(pravaMistnost);
        pravaMistnost.setVychod(vchod);
        prostredniMistnost.setVychod(skladiste);
        prostredniMistnost.setVychod(vchod);
        levaMistnost.setVychod(chodba);
        levaMistnost.setVychod(vchod);
        chodba.setVychod(levaMistnost);
        chodba.setVychod(skladiste);
        chodba.setVychod(dvere);
        skladiste.setVychod(chodba);
        skladiste.setVychod(prostredniMistnost);
        dvere.setVychod(poklad);
        dvere.setVychod(chodba);

        // Vytváří jednotlivé věci a truhly
        Vec zlatyKlic = new Vec("zlatý_klíč","tajuplný zlatý klíč ke dveřím", true);
        Vec stribrnyKlic = new Vec("stříbrný_klíč","tajuplný stříbrný klíč ke dveřím", true);
        Vec bronzovyKlic = new Vec("bronzový_klíč","tajuplný bronzový klíč ke dveřím", true);
        Truhla drevenaTruhla = new Truhla("dřevěná_truhla", "velká okovaná dřevěná truhla", true);
        Truhla zeleznaTruhla = new Truhla("železná_truhla","velká železná truhla",true);
        Truhla kamennaTruhla = new Truhla("kamenná_truhla", "velká kamenná truhla", true);
        Vec vaza = new Vec("váza", "antická váza s nádhernými rytinami", true);
        Vec mec = new Vec("meč", "zrezlý meč", true);
        Vec helma = new Vec("helma", "pozůstatek kožené helmy", true);



        Vec[] veci = {zlatyKlic, stribrnyKlic, bronzovyKlic, vaza, helma, mec};
        Truhla[] truhly = {drevenaTruhla, zeleznaTruhla, kamennaTruhla};
        Prostor[] prostory = {levaMistnost, prostredniMistnost, pravaMistnost, levaMistnost, chodba, skladiste};

        // náhodné rozmístění věcí v prostorech a truhlách
        umisteniVeci(veci, truhly, prostory);

        aktualniProstor = vchod;  // hra začíná u vchodu
    }

    /**
     * Metoda náhodně umisťuje věci do truhel a prostorů a truhly do prostotů.
     * Také náhodně nastavuje viditělnost věcí a truhel.
     *
     *@param veci truhly prostory. Pole obsahující věci, místnosti a prostory.
     *@return Vrací string s výpisem všech věcí v batohu.
     */

    private void umisteniVeci(Vec[] veci, Truhla[] truhly, Prostor[] prostory)
    {
        Random rand = new Random();
        int pozice;

        // náhodné nastavení viditelnosti a umístění vecí do prostorů nebo truhel
        for (int i = 0; i < veci.length; i++)
        {
            pozice = rand.nextInt(2);
            if(pozice == 0)
            {
                int truhla = rand.nextInt(truhly.length - 1);

                truhly[truhla].vlozVec(veci[i]);
            }

            if(pozice == 1)
            {
                int prostor = rand.nextInt(prostory.length - 1);
                int viditelnostVeci = rand.nextInt(2);

                veci[i].setViditelnost((viditelnostVeci == 1) ? true : false);
                prostory[prostor].pridejVecDoProstoru(veci[i]);
            }
        }

        // náhodné nastavení viditelnosti a umístění truhel do prostorů
        for (int i = 0; i < truhly.length; i++)
        {
            int index = rand.nextInt(prostory.length - 1);
            int viditelnostTruhly = rand.nextInt(2);

            truhly[i].setViditelnost((viditelnostTruhly == 1) ? true : false);
            prostory[index].pridejVecDoProstoru(truhly[i]);

        }
    }

    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

}
