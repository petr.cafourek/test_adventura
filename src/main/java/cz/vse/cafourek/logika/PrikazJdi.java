package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Avatar avatar;
    private Hra hra;
    private Batoh batoh;
    /**
    *  Konstruktor třídy
    *  
    *  @param plan, herní plán, ve kterém se bude ve hře "chodit"
     *               Avatar, kterému se budou snižovat životy
     *              hra, kterou při výhře ukončíme
     *              batoh, který zkontrolujeme pro výhru
    */    
    public PrikazJdi(HerniPlan plan, Avatar avatar, Hra hra, Batoh batoh) {
        this.plan = plan;
        this.avatar = avatar;
        this.hra = hra;
        this.batoh = batoh;
    }

    /**
     * Metoda "otevírá" dveře a zviditejňuje místnost s pokladem
     * Místnost s pokladem lze otevřít pouze tímto způsobem.
     *
     *@param plan na kterém se kontroluje zdali aktuální prostor je "dveře"
     *            batoh na kterém se kontroluje zdali obsahuje všechny 3 klíče
     */
    public boolean unlockDoor(HerniPlan plan, Batoh batoh)
    {
        if(plan.getAktualniProstor().getNazev() == "dveře" && (batoh.obsahujeBatohVec("zlatý_klíč")
                && batoh.obsahujeBatohVec("stříbrný_klíč")
                && batoh.obsahujeBatohVec("bronzový_klíč"))) {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }
        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = this.plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null || sousedniProstor.getViditelnost() == false) {
            return "Tam se odsud jít nedá!";
        }
        else {
            this.plan.setAktualniProstor(sousedniProstor);

            // místnost s pokladem je viditelná pouze pokud se otevřely dveře třemi klíči
            // pokud jsi došel so místnosti "poklad", tak jsi vyhrál
            if(this.plan.getAktualniProstor().getNazev() == "poklad")
            {
                this.hra.setKonecHry(true);
                return hra.vratViteznyEpilog();
            }
            else
            {
                // otevření dveří a zviditelnění východu k pokladu pokud jsi v místnosti "dveře" a máš všechny klíče
                if(unlockDoor(plan, batoh) == true)
                {
                    this.plan.getAktualniProstor().vratSousedniProstor("poklad").setViditelnost(true);
                    return sousedniProstor.getPopis() + "\n\n" + "Podařilo se ti otevřít dveře k pokladu.\n" +
                            "Nově objevený východ: poklad\n\n";

                }
                else
                {
                    nehodaAvatara(this.avatar); // implementování možnosti nehody při procházení
                    vysileniAvatara(this.avatar); // impementování vysílení avatara při procházení
                    return sousedniProstor.dlouhyPopis() + "\n\n" + sousedniProstor.vypisObsahProstoru();
                }
            }
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }

}
