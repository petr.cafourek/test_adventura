package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazPij implementuje pro hru příkaz pij.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */

public class PrikazPij implements IPrikaz {

    private static final String NAZEV = "pij";
    private Avatar avatar;
    Batoh batoh;

    /**
     *  Konstruktor třídy
     *
     *  @param avatar, kterému se bude snižovat žízeň.
     *                 batoh, u kterého se kontroluje zdali
     *                 obsahuje lahev a následné odebrání lahve
     */
    public PrikazPij(Avatar avatar, Batoh batoh)
    {
        this.avatar = avatar;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "pij". Pokud avatar má v batohu vec lahev,
     *  tak se žízeň avatara sníží na 0 a lahev se smaže z batohu.
     *  Pokud lahev už nemá, vypíše se chybové hlášení.
     *
     *@param parametry - bez parametrů
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {

        if(this.batoh.obsahujeBatohVec("lahev"))
        {
            this.batoh.odeberVecZBatohu("lahev");

            avatar.snizeniZizne(100);
            return "Vypitá láhev byla odebrána z batohu.";
        }
        else
        {
            return "Svoji vodu sis už vypil.";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
