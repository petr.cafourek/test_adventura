package cz.vse.cafourek.logika;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 * @version pro školní rok 2019/2020
 */
public class Prostor {

    private String nazev;
    private String popis;
    private Set<Prostor> vychody;   // obsahuje sousední místnosti
    private Map<String, Vec> obsahProstoru; // obsahuje veci a truhly v místnosti
    private boolean viditelnost;

    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     * @param viditelnost Viditelnost prostoru.
     */
    public Prostor(String nazev, String popis, boolean viditelnost) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        obsahProstoru = new HashMap<>();
        this.viditelnost = viditelnost;
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        this.vychody.add(vedlejsi);
    }

    public void pridejVecDoProstoru(Vec vec)
    {
        this.obsahProstoru.put(vec.getJmeno(), vec);
    }

    public Vec vezmiVec(String jmeno)
    {
        return this.obsahProstoru.remove(jmeno);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */

      @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return this.nazev;
    }


    public boolean getViditelnost() {
        return this.viditelnost;
    }

    public String getPopis()
    {
        return this.popis;
    }

    public void setViditelnost(boolean viditelnost) {
        this.viditelnost = viditelnost;
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v mistnosti/prostoru " + this.popis + ".\n"
                + popisVychodu();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        String vracenyText = "východy:";
        for (Prostor sousedni : this.vychody) {
            vracenyText += " " + sousedni.getNazev();
        }
        return vracenyText;
    }

    /**
     * Metoda vrací obsahProstoru.
     *
     *@return Map<String, Vec> obsah aktuálního prostoru.
     */
    public Map<String, Vec> getObsahProstoru()
    {
        return this.obsahProstoru;
    }

    public boolean jeVecTruhla(Vec vec)
    {
        if(vec instanceof Truhla)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Metoda vrací string se všemi viditelnými vecmi a truhlami v prostoru.
     *
     *@return Vrací string s výpisem všech věcí v prostoru.
     */
    public String vypisObsahProstoru()
    {
        boolean flag = false;
        String vypisProstoru = "V prostoru vidíte předměty:\n";
        for (Map.Entry<String,Vec> entry : this.obsahProstoru.entrySet())
        {
            if(entry.getValue().getViditelnost() == true)
            {
                flag = true;
                vypisProstoru += entry.getValue().getJmeno() + "\n";
            }
        }
        if(flag == true)
        {
            return vypisProstoru;
        }
        else
        {
            return vypisProstoru + "V prostoru žádné předměty nevidíš.\n";
        }

    }
    public boolean obsahujeProstorVec(String jmeno)
    {
        if(this.getObsahProstoru().containsKey(jmeno) && this.getObsahProstoru().get(jmeno).getViditelnost() == true)
        {
            return true;
        }
        else { return false; }
    }
    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory = 
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    public String setVychodyViditelne()
    {
        String objeveneVychody = "Objevené východy:\n";
        boolean flag = false;

        String jmenoProstoru = this.nazev;

        for (Prostor temp : this.vychody) {
            if(temp.getViditelnost() == false && temp.getNazev() != "poklad")
            {
                flag = true;
                temp.setViditelnost(true);
                objeveneVychody += temp.getNazev() + "\n";
            }

        }

        if(flag == false)
        {
            objeveneVychody += "Žádné nové východy nebyli objeveny\n";
        }
        return objeveneVychody;
    }

    public String setVeciVProstoruViditelne()
    {
        String objeveneVeci = "Objevené předměty: \n";
        boolean flag = false;

        for (Map.Entry<String,Vec> entry : this.obsahProstoru.entrySet())
        {
            if(entry.getValue().getViditelnost() == false)
            {
                entry.getValue().setViditelnost(true);
                objeveneVeci += entry.getKey() + "\n";
                flag = true;
            }
        }
        if(flag == false)
        {
            objeveneVeci += "Žádné nové předměty nebyli objeveny\n";
        }
        return objeveneVeci;
    }
}
