package cz.vse.cafourek.logika;


/**
 *  Třída implementující toto rozhraní bude ve hře zpracovávat jeden konkrétní příkaz.
 *  Toto rozhraní je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Petr Cafourek
 *@version    pro školní rok 2019/2020
 *  
 */
interface IPrikaz {
	
	/**
     *  Metoda pro provedení příkazu ve hře.
     *  Počet parametrů je závislý na konkrétním příkazu,
     *  např. příkazy konec a napoveda nemají parametry
     *  příkazy jdi, seber, polož mají jeden parametr
     *  příkaz pouzij může mít dva parametry.
     *  
     *  @param parametry počet parametrů závisí na konkrétním příkazu.
     *  
     */
    public String provedPrikaz(String... parametry);
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @return nazev prikazu
     */
	public String getNazev();

    /**
     *  Metoda náhodně vytváří "nehodu" avatara a nádledné snížení životů
     *
     *  @param avatar
     */
    public void nehodaAvatara(Avatar avatar);

    /**
     *  Metoda implementuje "vysílení" avatara a nádledné zvýšení žížně nebo snížení životů
     *
     *  @param avatar
     */
    public void vysileniAvatara(Avatar avatar);
}
