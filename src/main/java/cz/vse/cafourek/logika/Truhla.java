package cz.vse.cafourek.logika;

import java.util.Map;
import java.util.HashMap;


public class Truhla extends Vec {

    private Map<String, Vec> obsahTruhly;

    public Truhla(String jmeno, String popis, boolean viditelnost)
    {
        super(jmeno, popis, viditelnost);
        this.obsahTruhly = new HashMap<>();
    }

    public void vlozVec(Vec vec)
    {
        obsahTruhly.put(vec.getJmeno(), vec);
    }

    public Map<String, Vec> getObsahTruhly()
    {
        return this.obsahTruhly;
    }

    public int pocetVeciVTruhle()
    {
        return obsahTruhly.size();
    }
}
