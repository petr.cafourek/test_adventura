package cz.vse.cafourek.logika;

public class Vec {

    private String jmeno;
    private String popis;
    private boolean viditelnost;

    public Vec(String jmeno, String popis, boolean viditelnost) {
        this.jmeno = jmeno;
        this.popis = popis;
        this.viditelnost = viditelnost;
    }

    public String getJmeno() {
        return this.jmeno;
    }

    public String getPopis() {
        return this.popis;
    }

    public boolean getViditelnost() {
        return this.viditelnost;
    }

    public void setViditelnost(Boolean novaViditelnost) {
        this.viditelnost = novaViditelnost;
    }
}