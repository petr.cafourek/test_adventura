package cz.vse.cafourek.logika;

import java.util.Map;
import java.util.Random;

/**
 *  Třída PrikazOtevri implementuje pro hru příkaz napoveda.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */

public class PrikazOtevri implements IPrikaz {
    private static final String NAZEV = "otevři";
    private Avatar avatar;
    private HerniPlan plan;

    /**
     *  Konstruktor třídy
     *
     *  @param plan, herní plán, ve kterém se budou otevírat truhly
     *               Avatar, kterému se budou snižovat životy
     */
    public PrikazOtevri(HerniPlan plan, Avatar avatar)
    {
        this.plan = plan;
        this.avatar = avatar;
    }

    /**
     *  Provádí příkaz "otevři". Zkouší otevřít truhlu v místnosti. Pokud prostor
     *  obsahuje viditelnou truhlu, tak se její obsah přesune do obsahu prostoru
     *  pokud truhla není nebo není viditelná, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno truhly,
     *                         která se má otevřít.
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (Truhla), tak ....
            return "Co mám otevřít? Musíš zadat název truhly";
        }
        Prostor aktualniProstor = this.plan.getAktualniProstor();

        //kontrola zdali je truhla v místnosti a je viditelná
        if(aktualniProstor.getObsahProstoru().containsKey(parametry[0]) == false ||
                aktualniProstor.getObsahProstoru().get(parametry[0]).getViditelnost() == false)
        {
            return "V této místnosti není tato truhla";
        }

        // kontrola jestli je předmět instancí třídy Truhla
        if(aktualniProstor.jeVecTruhla(aktualniProstor.getObsahProstoru().get(parametry[0])) == true)
        {
            Truhla truhla = (Truhla) aktualniProstor.getObsahProstoru().get(parametry[0]);

            // pokud truhla není prázdná
            if(truhla.pocetVeciVTruhle() > 0) {
                for (Map.Entry<String, Vec> entry : truhla.getObsahTruhly().entrySet()) {

                    // přidání věcí z truhly do prostoru
                    aktualniProstor.pridejVecDoProstoru(entry.getValue());
                    System.out.printf("%s přidáno do prostoru.\n", entry.getKey());
                }
                // vymazání věcí z truhly
                truhla.getObsahTruhly().clear();

                vysileniAvatara(this.avatar);
                return "Věci z truhly byly přidány do prostoru a nyní je možné je uložit do batohu.";
            }
            // pokud truhla je prázdná
            else {
                vysileniAvatara(this.avatar);
                return "Truhla je prázdná.";
            }
        }
        else { return "Otevřít lze pouze truhlu"; }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
