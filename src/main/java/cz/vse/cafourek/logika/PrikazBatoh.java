package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazBatoh implementuje pro hru příkaz batoh.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Jarmila Pavlickova, Luboš Pavlíček, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */

public class PrikazBatoh implements IPrikaz{

    private static final String NAZEV = "batoh";
    private Avatar avatar;
    private Batoh batoh;

    /**
     *  Konstruktor třídy
     *
     *  @param avatar kterému se snižují životy při nehodách a vysílení a
     *                batoh se kterým se pracuje.
     */
    public PrikazBatoh(Avatar avatar, Batoh batoh)
    {
        this.avatar = avatar;
        this.batoh = batoh;
    }


    @Override
    public String provedPrikaz(String... parametry) {
        // volá se metoda vysileniAvatara()
        vysileniAvatara(this.avatar); // implementování vysílení avatara při příkazu batoh

        //vrací se string s obsahem batohu
        return this.batoh.vypisObsahBatohu();
    }

    /**
     * Metoda vrací název příkazu
     *
     *@return Vrací nazev příkazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
