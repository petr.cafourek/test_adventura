package cz.vse.cafourek.logika;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    pro školní rok 2019/2020
 */

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private HerniPlan herniPlan;
    private boolean konecHry = false;
    private Avatar avatar;
    private Batoh batoh;

    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        platnePrikazy = new SeznamPrikazu();
        batoh = new Batoh();
        avatar = new Avatar(this);

        platnePrikazy.vlozPrikaz(new PrikazNapoveda(this.platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(this.herniPlan, this.avatar, this, this.batoh));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazOtevri(this.herniPlan, this.avatar));
        platnePrikazy.vlozPrikaz(new PrikazPij(this.avatar, this.batoh));
        platnePrikazy.vlozPrikaz(new PrikazPoloz(this.herniPlan, this.avatar, this.batoh));
        platnePrikazy.vlozPrikaz(new PrikazProzkoumej(this.herniPlan, this.avatar));
        platnePrikazy.vlozPrikaz(new PrikazSpi(this.avatar));
        platnePrikazy.vlozPrikaz(new PrikazVezmi(this.herniPlan, this.avatar, this.batoh));
        platnePrikazy.vlozPrikaz(new PrikazBatoh(this.avatar, this.batoh));

        // vložení lahve, se kterou uživatel startuje.
        Vec lahev = new Vec("lahev", "lahev s vodou, ktere jsi si přinesl sebou",true);
        this.batoh.vlozVec(lahev);
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Jako hledač pokladů jste na své poslední expedici zavítal do tajuplného opuštěného chrámu.\n" +
                "Po vstoupení dovnitř se před vámi otevírají tři chodby, kterými se můžete dát.\n" +
                "Prozkoumejte chrám a najděte poklad, ale dejte si pozor!\n" +
                "Na stěně jste si všiml kresby, která varuje, že každého zloděje zde čeká smrt!\n\n" +
               herniPlan.getAktualniProstor().dlouhyPopis();
    }
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Děkuji za zahrání hry!";
    }

    public String vratViteznyEpilog()
    {
        return "Před vámi se otevírá bájná pokladnice hadího boha plná zaprášeých drahokamů a jiných cenností." +
                "\nGratuluji vyhrál jsi!!!\n";
    }
    /** 
     * Vrací true, pokud hra skončila.
     */
     public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
     public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        for(int i=0 ;i<parametry.length;i++){
           	parametry[i]= slova[i+1];  	
        }
        String textKVypsani=" .... ";
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
        }
        else {
            textKVypsani="Nevím co tím myslíš? Tento příkaz neznám.\n";
        }
        return textKVypsani + "\n";
    }
    
    
     /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
     /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
     public HerniPlan getHerniPlan(){
        return herniPlan;
     }
    
}

