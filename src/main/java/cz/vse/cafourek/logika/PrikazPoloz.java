package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazPoloz implementuje pro hru příkaz poloz.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */
public class PrikazPoloz implements IPrikaz {

    private static final String NAZEV = "polož";
    Avatar avatar;
    HerniPlan plan;
    Batoh batoh;

    /**
     *  Konstruktor třídy
     *
     *  @param plan, ze kterého se získává aktuální prostor
     *               avatar kterému se bude zvyšovat žízeň a přápadně snižovat životy.
     *                 batoh, ze kterého se přesune věc do prostoru
     */
    public PrikazPoloz(HerniPlan plan, Avatar avatar, Batoh batoh)
    {
        this.plan = plan;
        this.avatar = avatar;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "poloz". Pokud avatar má v batohu danou vec,
     *  tak se věc přesune z batohu do obsahu prostoru místnosti.
     *  Pokud danou vec nemá, vypíše se chybové hlášení.
     *
     *@param parametry - název věci
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry)
    {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (název věci), tak ....
            return "Co mám položit? Musíš zadat jméno věci";
        }

        // pokud avatar nemá danou věc v batohu
        if(this.batoh.obsahujeBatohVec(parametry[0]) == false)
        {
            return parametry[0] + " nemáš v batohu.";
        }
        Prostor aktualniProstor = this.plan.getAktualniProstor();

        // přidání odebrané věci z batohu do prostoru
        aktualniProstor.pridejVecDoProstoru(this.batoh.odeberVecZBatohu(parametry[0]));
        vysileniAvatara(this.avatar);
        return parametry[0] + " předmět byl odebrán z batohu u položen na zem prostoru.";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    /**
     * Metoda implementuje náhodnou nehodu avatara.
     * Pokud se stane nehoda, tak proběhne snížení životů avatara.
     *
     *@param avatar kterému se budou snižovat životy
     */
    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    /**
     * Metoda implementuje vysílení avatara.
     * Metoda zvyšuje žízeň avatara nebo pokud je žízeň na 100, tak snižuje životy avatara.
     *
     *@param avatar kterému se bude snižovat žízeň
     */
    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
