package cz.vse.cafourek.logika;

import java.util.Random;

/**
 *  Třída PrikazSpi implementuje pro hru příkaz spi.
 *  Tato třída je součástí jednoduché textové hry.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Petr Cafourek
 *@version    1.0
 *@created    květen 2020
 */
public class PrikazSpi implements IPrikaz{

    private static final String NAZEV = "spi";
    private Avatar avatar;

    /**
     *  Konstruktor třídy
     *
     *  @param avatar, kterému se bude snižovat žízeň.
     *                 batoh.
     */
    public PrikazSpi(Avatar avatar) {
        this.avatar = avatar;
    }

    /**
     *  Provádí příkaz "spi". Avatarovi zvyšuje životy a žízeň.
     *  Pokud uživatel zadá druhé slovo jako parametr,
     *  tak se vypíše chybová hláška.
     *
     *@param parametry - bez parametrů
     *@return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            return "Nechápu, proč jste zadal druhé slovo.";
        }
        else {
            this.avatar.zvyseniZivotu(20);
            this.avatar.zvyseniZizne(50);
            return "ZZZzzzZZZzzzZZZzzzZZZzzzZZZ";
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

    @Override
    public void nehodaAvatara(Avatar avatar) {
        Random rand = new Random();
        int sance = rand.nextInt(5);
        if(sance == 0)
        {
            System.out.print("Spustil jsi skrytou past v místnosti!\n");
            avatar.snizeniZivotu(25);
        }
    }

    @Override
    public void vysileniAvatara(Avatar avatar) {
        if(avatar.getZizen() == 100)
        {
            avatar.snizeniZivotu(10);
        }
        else
        {
            avatar.zvyseniZizne(10);
        }
    }
}
