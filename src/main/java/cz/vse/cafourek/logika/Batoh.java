package cz.vse.cafourek.logika;

import java.util.HashMap;
import java.util.Map;

/**
 *  Třída Batoh - třída představující batoh postavy do které lze ukladát a vyndavat věci.
 *
 *  Třída implmentuje metody na vkládání věcí do batohu a vyndavání věcí z batohu.
 *  Věc je vždy přesunuta z místnosti do batohu a naopak.
 *
 *@author     Petr Cafourek
 *@version    pro školní rok 2019/2020
 */

public class Batoh{

    private Map<String, Vec> obsahBatohu;
    private int kapacitaBatohu;

    public Batoh() {
        this.obsahBatohu = new HashMap<>();
        this.kapacitaBatohu = 4;
    }

    public Vec odeberVecZBatohu(String jmeno)
    {
        Vec odebranaVec = this.obsahBatohu.get(jmeno);
        this.obsahBatohu.remove(jmeno);
        return odebranaVec;
    }

    public boolean volneMistoBatoh()
    {
        if(this.obsahBatohu.size() < this.kapacitaBatohu)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Metoda vypisuje obsah batohu.
     *
     *@return Vrací string s výpisem všech věcí v batohu.
     */

    public String vypisObsahBatohu()
    {
        String retString = "Obsah batohu:\n";
        if(this.obsahBatohu.size() != 0)
        {
            for (Map.Entry<String,Vec> entry : this.obsahBatohu.entrySet())
            {
                retString += entry.getKey() + "\n";
            }
        }
        else
        {
            retString += "Batoh je prázdný";
        }
        return retString;
    }

    public boolean obsahujeBatohVec(String jmeno)
    {
        return this.obsahBatohu.containsKey(jmeno);
    }

    /**
     * Metoda vkládá věc do batohu, pokud je v batohu místo.
     *
     *@param vec kterou metoda vloží do batohu.
     */

    public void vlozVec(Vec vec)
    {

        if(this.obsahBatohu.size() < this.kapacitaBatohu)
        {
            this.obsahBatohu.put(vec.getJmeno(), vec);
        }
        else
        {
            System.out.printf("%s nelze do batohu vložit.\nBatoh je plný.", vec.getJmeno());
        }
    }
}
