package cz.vse.cafourek.logika;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class BatohTest {

    private Vec vec1;
    private Vec vec2;
    private Vec vec3;
    private Vec vec4;
    private Batoh batoh;

    @Test
    public void testVlozVecDoBatohu() {
        batoh = new Batoh();

        this.vec1 = new Vec("lahev", "lahev s vodou, ktere jsi si přinesl sebou",true);
        this.vec1 = new Vec("lahev", "lahev s vodou, ktere jsi si přinesl sebou",true);
        this.vec2 = new Vec("zlatý_klíč","tajuplný zlatý klíč ke dveřím", true);
        this.vec3 = new Vec("stříbrný_klíč","tajuplný stříbrný klíč ke dveřím", true);
        this.vec4 = new Vec("bronzový_klíč","tajuplný bronzový klíč ke dveřím", true);
        this.vec4 = new Vec("váza", "antická váza s nádhernými rytinami", true);

        this.batoh.vlozVec(this.vec1);
        this.batoh.vlozVec(this.vec2);
        this.batoh.vlozVec(this.vec3);
        this.batoh.vlozVec(this.vec4);

        Assert.assertEquals(true, this.batoh.obsahujeBatohVec("lahev"));
        Assert.assertEquals(false, this.batoh.obsahujeBatohVec("nevim"));
        Assert.assertEquals(true, this.batoh.obsahujeBatohVec("zlatý_klíč"));
        Assert.assertEquals(true, this.batoh.obsahujeBatohVec("stříbrný_klíč"));
        Assert.assertEquals(false, this.batoh.obsahujeBatohVec("bronzový_klíč"));
    }
}