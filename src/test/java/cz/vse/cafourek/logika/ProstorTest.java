package cz.vse.cafourek.logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    Jarmila Pavlíčková
 * @version   pro skolní rok 2016/2017
 */
public class ProstorTest
{
    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře, 
     */
    @Test
    public  void testLzeProjit() {		
        Prostor prostor1 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě", true);
        Prostor prostor2 = new Prostor("bufet", "bufet, kam si můžete zajít na svačinku", false);
        prostor1.setVychod(prostor2);
        prostor2.setVychod(prostor1);
        assertEquals(prostor2, prostor1.vratSousedniProstor("bufet"));
        assertEquals(null, prostor2.vratSousedniProstor("pokoj"));
    }

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře,
     */
    @Test
    public  void testViditelnostPokladu() {
        Prostor dvere = new Prostor("dveře", "Našel jsi zamčené dveře. Po bližším prozkoumání rytin na " +
                "dveřích se ukázalo, že je potřeba najít tři klíče.\n" +
                "Jenom pokud člověk prokáže sílu svého ducha a získá " +
                "tři klíče, tak bude moci otevřít dveře a získat poklad", true);
        Prostor poklad = new Prostor("poklad", "bájná pokladnice hadího boha", false);

        Vec zlatyKlic = new Vec("zlatý_klíč","tajuplný zlatý klíč ke dveřím", true);
        Vec stribrnyKlic = new Vec("stříbrný_klíč","tajuplný stříbrný klíč ke dveřím", true);
        Vec bronzovyKlic = new Vec("bronzový_klíč","tajuplný bronzový klíč ke dveřím", true);

        dvere.setVychod(poklad);
        poklad.setVychod(dvere);

        dvere.setVeciVProstoruViditelne();

        assertEquals(false, poklad.getViditelnost());
    }
}
