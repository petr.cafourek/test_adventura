package cz.vse.cafourek.logika;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HraTest {

    private Hra hra1;

    @BeforeEach
    void setUp() {
            this.hra1 = new Hra();
    }

    @Test
    public void testPohybuVprostorech()
    {
        Assert.assertEquals("vchod", this.hra1.getHerniPlan().getAktualniProstor().getNazev());
        this.hra1.zpracujPrikaz("jdi prostřední_místnost");
        Assert.assertEquals(false, this.hra1.konecHry());
        Assert.assertEquals("prostřední_místnost", this.hra1.getHerniPlan().getAktualniProstor().getNazev());
        this.hra1.zpracujPrikaz("jdi skladiště");
        Assert.assertEquals(false, this.hra1.konecHry());
        Assert.assertEquals("prostřední_místnost", this.hra1.getHerniPlan().getAktualniProstor().getNazev());
        this.hra1.zpracujPrikaz("konec");
        Assert.assertEquals(true, this.hra1.konecHry());
    }
}