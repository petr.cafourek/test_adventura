package cz.vse.cafourek.logika;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author    Luboš Pavlíček
 * @version   pro školní rok 2016/2017
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private Avatar avatar;
    private Batoh batoh;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazOtevri prikazOtevri;
    private PrikazPij prikazPij;
    private PrikazPoloz prikazPoloz;
    private PrikazBatoh prikazBatoh;
    private PrikazVezmi prikazVezmi;
    private PrikazSpi prikazSpi;
    private PrikazProzkoumej prikazProzkoumej;
    
    @Before
    public void setUp() {
        hra = new Hra();
        avatar = new Avatar(hra);
        batoh = new Batoh();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan(), avatar, hra, batoh);
        prikazOtevri = new PrikazOtevri(hra.getHerniPlan(), avatar);
        prikazPij = new PrikazPij(avatar, batoh);
        prikazPoloz = new PrikazPoloz(hra.getHerniPlan(), avatar, batoh);
        prikazBatoh = new PrikazBatoh(avatar, batoh);
        prikazVezmi = new PrikazVezmi(hra.getHerniPlan(), avatar, batoh);
        prikazSpi = new PrikazSpi(avatar);
        prikazProzkoumej = new PrikazProzkoumej(hra.getHerniPlan(), avatar);


    }

    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(null, seznPrikazu.vratPrikaz("nápověda"));
    }
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prikazPoloz);
        seznPrikazu.vlozPrikaz(prikazProzkoumej);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("nápověda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("polož"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("prozkoumej"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("batoh"));
    }
    
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prikazPoloz);
        seznPrikazu.vlozPrikaz(prikazProzkoumej);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(false, nazvy.contains("nápověda"));
        assertEquals(false, nazvy.contains("Konec"));
        assertEquals(false, nazvy.contains("spi"));
        assertEquals(true, nazvy.contains("polož"));
        assertEquals(true, nazvy.contains("prozkoumej"));
    }
    
}
